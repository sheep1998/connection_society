// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import store from './store/store.js'
import axios from 'axios'
import qs from 'qs'
import ECharts from 'vue-echarts'
import WebRTC from 'vue-webrtc'
import 'bootstrap/dist/css/bootstrap.min.css'
import 'bootstrap/dist/js/bootstrap.min'

Vue.config.productionTip = false
Vue.prototype.$axios = axios
Vue.prototype.$qs = qs
Vue.use(ElementUI);
Vue.use(WebRTC)
Vue.component('v-chart', ECharts)

new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})
