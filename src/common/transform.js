var transform = {
  activityMap:{
    "射击比赛":"https://ss1.bdstatic.com/70cFvXSh_Q1YnxGkpoWK1HF6hhy/it/u=3893801765,3520681590&fm=26&gp=0.jpg",
    "狼人杀":"https://gimg2.baidu.com/image_search/src=http%3A%2F%2F5b0988e595225.cdn.sohucs.com%2Fimages%2F20171207%2Fee58d7d87b124af3b8408c809fe5c83f.jpeg&refer=http%3A%2F%2F5b0988e595225.cdn.sohucs.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1611457921&t=e77c71fc4cb26baaafd226a52c6a8af1",
    "2020级迎新晚会":"https://ss1.bdstatic.com/70cFvXSh_Q1YnxGkpoWK1HF6hhy/it/u=2813506312,2118383029&fm=26&gp=0.jpg"
  },
  getLabel(desc){
    var labels = []
    var keyWord = {
      "迎新晚会":["社交","校园","晚会","SJTU"],
      "射击":["真人游戏","比赛","射击"],
      "狼人杀":["桌游"]
    }
    for(let key in keyWord){
      if(desc.indexOf(key)!=-1){
        labels = labels.concat(keyWord[key])
      }
    }
    return labels
  }
}
module.exports = transform;
