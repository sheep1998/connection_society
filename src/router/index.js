import Vue from 'vue'
import Router from 'vue-router'
import AllActivities from '@/components/AllActivities'
import AllPlace from "@/components/AllPlace";
import PostActivity from '@/components/PostActivity'
import ActivityDetail from "@/components/ActivityDetail";
import PostPlace from "../components/PostPlace";
import PlaceDetail from "../components/PlaceDetail";
import Person from "../components/Person";
import Statistic from "../components/Statistic";
import MyActivities from "../components/MyActivities";
import FaceTime from '../components/FaceTime'
Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      redirect:'/allPlace',
      name:'allPlace',
      component: AllPlace
    },
    {
      path: '/allPlace',
      name:'allPlace',
      component: AllPlace
    },
    {
      path: '/postPlace',
      name:'postPlace',
      component: PostPlace
    },
    {
      path: '/placeDetail',
      name:'placeDetail',
      component: PlaceDetail
    },
    {
      path:'/allActivities',
      name:'allActivities',
      component: AllActivities
    },
    {
      path:'/postActivity',
      name:"postActivity",
      component: PostActivity
    },
    {
      path:'/activityDetail',
      name:"activityDetail",
      component: ActivityDetail
    },
    {
      path:'/person',
      name:'person',
      component: Person
    },
    {
      path:'/statistic',
      name:'statistic',
      component: Statistic
    },
    {
      path:'/myActivities',
      name:'myActivities',
      component: MyActivities
    },
    {
      path:'/faceTime',
      name:'faceTime',
      component: FaceTime
    }

  ]
})
